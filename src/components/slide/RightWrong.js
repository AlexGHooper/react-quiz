// This component renders the tick or cross upon answer selection

import React from "react";
import PropTypes from "prop-types";

const RightWrong = ({ correct }) => (
  <div
    className={`right-wrong ${
      correct ? "right-wrong--right" : "right-wrong--wrong"
    }`}
  />
);

// Prop Validation
RightWrong.propTypes = {
  correct: PropTypes.number.isRequired
};

export default RightWrong;
