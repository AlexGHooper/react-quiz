// This Component renders the answer copy
import React from "react";
import PropTypes from "prop-types";

const AnswerCopy = ({ answer, copy }) => (
  <div className="QandA">
    <div className="QandA__inner">
      <span>
        <strong>
          The correct answer is
          <span> {answer}. </span>
        </strong>
        {
          // I realise the below is not best practice, however since I know what
          // the data is I'm comfortable encoding the strings
        }
        <span dangerouslySetInnerHTML={{ __html: copy }} />
      </span>
    </div>
  </div>
);

// Prop Validation
AnswerCopy.propTypes = {
  answer: PropTypes.string.isRequired,
  copy: PropTypes.string
};
// Default Props
AnswerCopy.defaultProps = {
  copy: "Some more information about the answer should go here"
};

export default AnswerCopy;
