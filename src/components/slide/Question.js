// This Component renders the question

import React from "react";
import PropTypes from "prop-types";

const Question = ({ question }) => (
  <div className="QandA">
    <div className="QandA__inner">{question}</div>
  </div>
);

// Prop Validation
Question.propTypes = {
  question: PropTypes.string.isRequired
};

export default Question;
