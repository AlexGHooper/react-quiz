// This Component renders the image of the current question, taking in the Progress and RightWrong Components
// to make my responisve design easier
import React from "react";
import PropTypes from "prop-types";

const Image = ({ img, children }) => {
  const background = {
    backgroundImage: `url(${img})`
  };
  return (
    <div className="bg-image-container">
      <div className="bg-image-container__inner" style={background} />
      {children}
    </div>
  );
};

// Prop Validation
Image.propTypes = {
  img: PropTypes.string.isRequired,
  children: PropTypes.instanceOf(Array).isRequired
};

export default Image;
