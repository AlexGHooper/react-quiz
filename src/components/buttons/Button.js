// This Component renders a button with an onClick event depending on what prop was passed
import React from "react";
import PropTypes from "prop-types";

// Import Styles
import "./Button.css";

const Button = ({ event, children }) => (
  <div className="button" onClick={() => event()}>
    {children}
  </div>
);

// Prop Validation
Button.propTypes = {
  event: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired
};

export default Button;
