export var craghoppersSlidesQandA = [
  {
    slide: "1",
    quesiton: "Where would you see this?",
    answers: ["America", "Turkey", "Italy", "Wales"],
    correctAnswer: "Turkey",
    answerCopy:
      "These are the so-called &ldquo;Fairy Chimneys&rdquo; of Cappadocia, formed through wind and rain erosion of porous volcanic rock. Locals carved homes, churches, indeed entire towns into them.",
    imagePath: "./1.jpg"
  },
  {
    slide: "2",
    quesiton: "Where would you see this?",
    answers: ["Italy", "Guatemala", "Uganda", "Chad"],
    correctAnswer: "Italy",
    answerCopy:
      "This is the Cascate delle Marmore in Umbria, an artificial waterfall built by the Romans to divert a troublesome river away from two towns.",
    imagePath: "./2.jpg"
  },
  {
    slide: "3",
    quesiton: "Where would you see this?",
    answers: ["Ethiopia", "Malaysia", "Belgium", "The Philippines"],
    correctAnswer: "The Philippines",
    answerCopy:
      "These are the &ldquo;Chocolate Hills&rdquo; on the island of Bohol: cones of limestone karst covered in grass that turns brown in autumn - hence the name.",
    imagePath: "./3.jpg"
  },
  {
    slide: "4",
    quesiton: "Where would you see this?",
    answers: ["Scotland", "Thailand", "China", "Madagascar"],
    correctAnswer: "Thailand",
    answerCopy:
      "This is the 66ft-tall limestone islet Khao Ta-Pu, part of Ao Phang Na national park near Phuket. It was seen in The Man With The Golden Gun and Tomorrow Never Dies, so is often called &ldquo;James Bond Island&rdquo;.",
    imagePath: "./4.jpg"
  },
  {
    slide: "5",
    quesiton: "Where would you see this?",
    answers: ["South Korea", "America", "Norway", "The Netherlands"],
    correctAnswer: "Norway",
    answerCopy:
      "This is the Kjeragbolten, a boulder wedged atop a 3,228ft-deep gorge in the county of Rogaland. Long lines form of tourists wanting to pose for photos on it.",
    imagePath: "./5.jpg"
  },
  {
    slide: "6",
    quesiton: "Where would you see this?",
    answers: ["The Orkneys", "Shetland", "The Hebrides", "Dorset"],
    correctAnswer: "The Orkneys",
    answerCopy:
      "This is the 449ft sandstone stack known as the Old Man Of Hoy. It used to look a lot more man-shaped, with two legs and shoulders, until a partial collapse in the early 19th century.",
    imagePath: "./6.jpg"
  },
  {
    slide: "7",
    quesiton: "Where would you see this?",
    answers: ["America", "Chad", "Mexico", "France"],
    correctAnswer: "Chad",
    answerCopy:
      "This is the 394ft  Aloba Arch, part of the Ennedi Range in the northeast of this desert country. It&rsquo;s thought to be the largest sandstone arch outside America&rsquo;s Colorado Plateau.",
    imagePath: "./7.jpg"
  },
  {
    slide: "8",
    quesiton: "Where would you see this?",
    answers: ["Vietnam", "Ireland", "Finland", "Canada"],
    correctAnswer: "Canada",
    answerCopy:
      "This is Balancing Rock, a 30ft black basalt stack column that seems to balance precariously over the sea near Digby, Nova Scotia.",
    imagePath: "./8.jpg"
  },
  {
    slide: "9",
    quesiton: "Where would you see this?",
    answers: ["Scotland", "Luxembourg", "Switzerland", "Kenya"],
    correctAnswer: "Scotland",
    answerCopy:
      "This is the walking track up Ben Nevis, the UK&rsquo;s highest peak. It takes about three to five hours to reach the top. ",
    imagePath: "./9.jpg"
  },
  {
    slide: "10",
    quesiton: "Where would you see this?",
    answers: ["America", "Canada", "Ethiopia", "Estonia"],
    correctAnswer: "America",
    answerCopy:
      "This is the 1,267ft igneous rock formation in Wyoming known as Devil&rsquo;s Tower, which famously features in the film Close Encounters Of The Third Kind. ",
    imagePath: "./10.jpg"
  },
  {
    slide: "11",
    quesiton: "Where would you see this?",
    answers: [
      "Smoky mountains",
      "Black mountains",
      "Ural mountains",
      "Grampian mountains"
    ],
    correctAnswer: "Ural mountains",
    answerCopy:
      "These are the Man-Pupu-Nyer or &ldquo;Seven Strong Men&rdquo; natural monoliths in the Komi Republic, part of the Russian Federation. They are said to be evil giants turned to stone.",
    imagePath: "./11.jpg"
  },
  {
    slide: "12",
    quesiton: "Where would you see this?",
    answers: ["Italy", "Germany", "Turkey", "China"],
    correctAnswer: "China",
    answerCopy:
      "This is the &ldquo;Stone Forest&rdquo; (Shilin in Chinese) of water-eroded limestone columns in Yunnan province. It&rsquo;s a Unesco world heritage site.",
    imagePath: "./12.jpg"
  },
  {
    slide: "13",
    quesiton: "Where would you see this?",
    answers: ["Iceland", "Ireland", "Monaco", "Kosovo"],
    correctAnswer: "Iceland",
    answerCopy:
      "This is Svartifoss, the &ldquo;black falls&rdquo; in Vatnajökull national park. Its hexagonal basalt columns were formed by cooling lava, in the same way as the Giant&rsquo;s Causeway in Northern Ireland.",
    imagePath: "./13.jpg"
  },
  {
    slide: "14",
    quesiton: "Where would you see this?",
    answers: ["America", "Australia", "Austria", "Hungary"],
    correctAnswer: "Australia",
    answerCopy:
      "This is the 46ft granite &ldquo;inselberg&rdquo; known as Wave Rock, in the Hyden Wildlife Park east of Perth. Its flared slopes were caused by chemical erosion by groundwater.",
    imagePath: "./14.jpg"
  }
];

export default craghoppersSlidesQandA;
