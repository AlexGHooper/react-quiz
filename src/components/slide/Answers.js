// This Component will render (map) through answers and listen for click

import React from "react";
import PropTypes from "prop-types";

const Answers = ({ answers, checkAnswer }) => (
  <div className="answer">
    {answers.map(answer => (
      <div
        key={answer}
        role="button"
        className="answer__button"
        onClick={() => checkAnswer(answer)}
      >
        {answer}
      </div>
    ))}
  </div>
);

// Prop Validation
Answers.propTypes = {
  answers: PropTypes.instanceOf(Array).isRequired,
  checkAnswer: PropTypes.func.isRequired
};

export default Answers;
