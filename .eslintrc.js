module.exports = {
  extends: "airbnb",
  rules: {
    "react/jsx-filename-extension": [1, { extensions: [".js"] }],
    "react/prefer-stateless-function": 0,
    "comma-dangle": 0,
    "arrow-body-style": 0,
    "arrow-parens": 0,
    "no-console": 0,
    globals: { fetch: false },
    env: {
      browser: 0,
      node: 0,
      jasmine: 0
    },
    quotes: 0
  }
};
