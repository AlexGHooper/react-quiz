import React from "react";

// Import data
import { craghoppersSlidesQandA } from "./data/data";

// Import Components
import Slide from "./components/slide/Slide";
import IntroSlide from "./components/unique_slides/IntroSlide";
import FinalSlide from "./components/unique_slides/FinalSlide";

// Import styles
import "./App.css";

export default class App extends React.Component {
  constructor() {
    super();
    // Set State
    this.state = {
      qData: craghoppersSlidesQandA,
      currentScore: 0,
      currentSlide: 0,
      totalSlides: 0,
      slideWidth: 0,
      slidePosition: 0
    };
    // Bind functions
    this.renderSlides = this.renderSlides.bind(this);
    this.calcTotalSlides = this.calcTotalSlides.bind(this);
    this.getSlideWidth = this.getSlideWidth.bind(this);
    this.nextSlide = this.nextSlide.bind(this);
    this.addToScore = this.addToScore.bind(this);
    this.retakeQuiz = this.retakeQuiz.bind(this);
    this.handleResize = this.handleResize.bind(this);
    // Refs
    this.container = React.createRef();
  }

  componentDidMount() {
    // When component mounts, calculate the total number of slides
    this.calcTotalSlides(this.state.qData);
    // Fire handleResize on mount
    this.handleResize();
  }

  getSlideWidth(slide) {
    // Gets the width of the container and sets to state
    let slideWidth = slide;
    if (slideWidth > 946) {
      slideWidth = 946;
    }
    this.setState({ slideWidth });
  }

  handleResize() {
    // Ensures that current slide fits in viewport
    // Listen for resize and fire func
    window.addEventListener("resize", this.handleResize);
    // Get width of container
    this.getSlideWidth(this.container.current.clientWidth);
    // Get new width - times that by the current slide number and assign that value to the transform
    let width = this.state.slideWidth;
    const slidePosition = -Math.abs(width * this.state.currentSlide);
    this.setState({ slidePosition });
  }

  addToScore() {
    // Increments score by 1
    let currentScore = this.state.currentScore;
    currentScore += 1;
    this.setState({ currentScore });
  }

  calcTotalSlides(data) {
    // Calculates the total number of slides based on the data obtained
    const totalSlides = data.length;
    this.setState({ totalSlides });
  }

  nextSlide() {
    // Increments page number, calculates the value to transform the container to reveal next slide
    let currentSlide = this.state.currentSlide;
    let width = this.state.slideWidth;
    let slidePosition = this.state.slidePosition;
    currentSlide += 1;
    slidePosition -= width;
    this.setState({ currentSlide, slidePosition });
  }

  retakeQuiz() {
    // Reloads quiz
    window.location.reload();
  }

  renderSlides() {
    // Maps through slide data and outputs Slide component
    const { qData, totalSlides, currentSlide } = this.state;
    return qData.map(slide => (
      <Slide
        key={`slide-${slide.slide}`}
        answers={slide.answers}
        correctAnswer={slide.correctAnswer}
        answerCopy={slide.answerCopy}
        question={slide.quesiton}
        currentSlide={currentSlide}
        img={slide.imagePath}
        totalSlides={totalSlides}
        nextSlide={this.nextSlide}
        addToScore={this.addToScore}
      />
    ));
  }

  render() {
    // Sets up container and renders slides
    const { slidePosition, currentScore, totalSlides } = this.state;
    return (
      <div className="container" ref={this.container}>
        <div
          className="slidesContainer"
          style={{
            transform: `translate3d(${slidePosition}px, 0px, 0px)`
          }}
        >
          <IntroSlide nextSlide={this.nextSlide} />
          {this.renderSlides()}
          <FinalSlide
            finalScore={currentScore}
            totalQuestions={totalSlides}
            retakeQuiz={this.retakeQuiz}
          />
        </div>
      </div>
    );
  }
}
