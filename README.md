# Alex Quiz test — Breakdown

## Setup

Simply clone the repo, yarn/npm install then npm/yarn start to start local environment

## Overview

Before I get into a breakdown of the project, I wanted to note that I found this entire process very enjoyable, and I’ve learnt a few things along the way. Please see below for access to repo and a live build version:

Bitbucket: https://bitbucket.org/BeyondLuxury/react-quiz
Netlify Droplet: https://clever-swanson-3026ca.netlify.com/

I’d like to summarise why I chose to build this in React, before touching on what I’d do if I built this app again.

### My Approach - React

My workflow was made up of 4 parts:

1. Planning
2. Setup of core logic
3. Styling
4. Testing

#### 1) Planning

I considered building this project using vanilla js, however, as I began to break the task into sections it became clear that a component-based framework such as React would be the best strategy, not only from a component perspective but also as it’s a UI rich app that could have many UI states, which React makes simpler to work with. From the video brief, I could visualise several parts of the quiz that would benefit from being components. Each “question” is broken down into a “Slide” component, which has several components of its own. Below is a diagram explaining how I broke the separate parts down into components:

![alt text](https://i.imgur.com/uwK6TFL.png "Component Breakdown")

I decided to go with a slider approach, with all slides rendering on page load.

#### 2) Setup of core logic

Once I’d created each component, I started to create the logic for the app. I decided to modify the data file to include the paths to the images, so I could easily map through each array item; send that data to the ‘Slide’ via props; and then have the slide render the image. I also optimised the pngs for web (app size is 1.7mb). Throughout, I tried to utilise ES6 to my advantage, in the form of arrow functions, template literals, destructuring etc.

#### 3) Styling

Once I had all of the logic working, I started to style the app in accordance with the video brief and to work responsively. I made one slight change: when the user selects a correct answer, the tick element has a green background. A user will naturally associate green with a correct answer, as opposed to keeping both options orange/red. Apart from that change, I tried to follow the brief as much as I could with the time allotted.

#### 4) Testing

Once most styling and logic was complete, I started testing on several mobile devices (iOS / Android) and also used BrowserStack to test across different Browser versions.

### Other Approaches

Having finished the task, there are a few things I’d do differently if I had more time / started again

- Used React styled components to clean-up code
- Used CSS variables
- Refactored the “Slide” toggle functions into one conditional statement (instead of several different ones)
- Rather than using a slider approach, I would mount each question and ‘fake’ the slide look, reducing the need for several resize calculations/logic.
- A small thing, but it annoys me: when the user resizes the browser the transform changes cause the slide to jump slightly. A fix for this would be to remove any transitions when the window is being resized.
- Clean up animations and implement better ‘click’ animations on the answer click.
- General JavaScript optimisation where possible
- Improve how it looks on mobile

### Final Notes

Although there are some aspects I’d like to improve on if I had more time, I believe I’ve produced something pretty close to the brief. If you have any questions regarding the project, please don’t hesitate to ask.

Thanks,
Alex
