// This component renders the final slide and changes output depending on score

import React from "react";
import PropTypes from "prop-types";

// Import components
import Button from "../buttons/Button";

// Import Image
import background from "../../assets/images/end.png";

const styles = {
  backgroundImage: `url(${background})`
};

const FinalSlide = ({ finalScore, totalQuestions, retakeQuiz }) => (
  <div className="slide last" style={styles}>
    <div className="slide__container">
      <div className="title">
        {finalScore > 10 ? "Great Job!" : "Bad luck!"}
      </div>
      <div className="body-copy">
        {`You answered ${finalScore} out of ${totalQuestions} questions correctly.`}
        {finalScore > 10
          ? " You really do know every wild place in the world after all!"
          : " You don't know every wild place in the world after all"}
      </div>

      <Button event={retakeQuiz}>Retake quiz</Button>
    </div>
  </div>
);

// Prop Validation
FinalSlide.propTypes = {
  finalScore: PropTypes.number.isRequired,
  totalQuestions: PropTypes.number,
  retakeQuiz: PropTypes.func.isRequired
};
// Default Props
FinalSlide.defaultProps = {
  totalQuestions: 14
};

export default FinalSlide;
