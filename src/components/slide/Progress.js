// This Component renders the progress of the quiz

import React from "react";
import PropTypes from "prop-types";

const Progress = ({ current, total }) => (
  <div className="progress">
    <div className="progress__inner">
      <div className="question-number">{current}</div>
      <div className="question-total">{total}</div>
    </div>
  </div>
);

// Prop Validation
Progress.propTypes = {
  current: PropTypes.number.isRequired,
  total: PropTypes.number
};
// Default Props
Progress.defaultProps = {
  total: 14
};

export default Progress;
