// This Component renders the Next Slide button and takes the nextSlide func as a prop
import React from "react";
import PropTypes from "prop-types";

const NextSlide = ({ nextSlide }) => (
  <div className="next-slide" role="button" onClick={() => nextSlide()} />
);

// Prop Validation
NextSlide.propTypes = {
  nextSlide: PropTypes.func.isRequired
};

export default NextSlide;
