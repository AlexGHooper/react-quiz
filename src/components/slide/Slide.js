// This component outputs all of the slide details and handles some logic such as checking answers

// Import React reqs
import React, { Component } from "react";
import PropTypes from "prop-types";

// Import animation library
import posed, { PoseGroup } from "react-pose";

// Import individul components
import Answers from "./Answers";
import Question from "./Question";
import AnswerCopy from "./AnswerCopy";
import Progress from "./Progress";
import NextSlide from "../buttons/NextSlide";
import Image from "./Image";
import RightWrong from "./RightWrong";

// Import Styles
import "./Slide.css";

// Pose Style Divs
const RightWrongAnimated = posed.div({
  enter: { opacity: 1, scale: 1, transition: { delay: 100, duration: 1000 } },
  exit: { opacity: 0, scale: 0.1 }
});

const AnswersAnimated = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0, transition: { delay: 1200, duration: 600 } }
});

const NextSlideAnimated = posed.div({
  enter: { opacity: 1, transition: { delay: 1700, duration: 300 } },
  exit: { opacity: 0 }
});

const QuestionAnimated = posed.div({
  enter: { opacity: 1 },
  exit: { y: -100, opacity: 0, transition: { delay: 1700, duration: 400 } }
});

const CopyAnimated = posed.div({
  enter: { opacity: 1, y: 0, transition: { delay: 2000, duration: 400 } },
  exit: { opacity: 0, y: -100 }
});

export default class Slide extends Component {
  constructor() {
    super();
    // Setup an answered & correct state
    this.state = {
      answered: 0,
      correct: 0
    };
    // Bind functions to this
    this.checkAnswer = this.checkAnswer.bind(this);
    this.toggleSlideButton = this.toggleSlideButton.bind(this);
    this.toggleRightWrong = this.toggleRightWrong.bind(this);
    this.toggleAnswers = this.toggleAnswers.bind(this);
    this.toggleQuestionAnswerCopy = this.toggleQuestionAnswerCopy.bind(this);
  }

  checkAnswer(answer) {
    // Check if this answer is correct
    let correct = this.state.correct;
    if (answer === this.props.correctAnswer) {
      this.props.addToScore();
      correct = 1;
      this.setState({ correct });
    }
    let answered = this.state.answered;
    answered = 1;
    this.setState({ answered });
  }

  toggleSlideButton() {
    // Reveals Next Slide button if the question has been answered
    if (this.state.answered) {
      return (
        <NextSlideAnimated
          key="NextSlideAnimated"
          className="next-slide-container"
        >
          <NextSlide nextSlide={this.props.nextSlide} />
        </NextSlideAnimated>
      );
    }
  }

  toggleRightWrong() {
    // Reveals the right or wrong circle if the question has been answered
    if (this.state.answered) {
      return (
        <RightWrongAnimated key="RightWrongAnimated" className="right-wrong">
          <RightWrong correct={this.state.correct} />
        </RightWrongAnimated>
      );
    }
  }

  toggleAnswers() {
    // Hides Next answers if the question has been answere
    if (!this.state.answered) {
      return (
        <AnswersAnimated key="AnswersAnimated" className="answer-container">
          <Answers
            answers={this.props.answers}
            checkAnswer={this.checkAnswer}
          />
        </AnswersAnimated>
      );
    }
  }

  toggleQuestionAnswerCopy() {
    if (!this.state.answered) {
      return (
        <QuestionAnimated key="QuestionAnimated" className="QandA-container">
          <Question question={this.props.question} />
        </QuestionAnimated>
      );
    } else {
      return (
        <CopyAnimated key="CopyAnimated" className="QandA-container">
          <AnswerCopy
            answer={this.props.correctAnswer}
            copy={this.props.answerCopy}
          />
        </CopyAnimated>
      );
    }
  }

  render() {
    // prop destructure
    const { img, currentSlide, totalSlides } = this.props;

    return (
      // Render each slide with Data
      <div className="slide">
        <PoseGroup>{this.toggleQuestionAnswerCopy()}</PoseGroup>
        <Image img={img}>
          <PoseGroup>{this.toggleRightWrong()}</PoseGroup>
          <Progress current={currentSlide} total={totalSlides} />
        </Image>
        <PoseGroup>{this.toggleAnswers()}</PoseGroup>
        <PoseGroup>{this.toggleSlideButton()}</PoseGroup>
      </div>
    );
  }
}
// Prop Validation
Slide.propTypes = {
  answers: PropTypes.instanceOf(Array).isRequired,
  correctAnswer: PropTypes.string.isRequired,
  answerCopy: PropTypes.string,
  question: PropTypes.string.isRequired,
  currentSlide: PropTypes.number.isRequired,
  img: PropTypes.string.isRequired,
  totalSlides: PropTypes.number.isRequired,
  nextSlide: PropTypes.func.isRequired,
  addToScore: PropTypes.func.isRequired
};
// Default Props
Slide.defaultProps = {
  answerCopy: "Some more information about the Answer goes here"
};
