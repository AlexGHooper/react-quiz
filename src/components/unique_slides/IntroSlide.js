// This Component renders the first slide

import React from "react";
import PropTypes from "prop-types";

// Import Components
import Button from "../buttons/Button";

// Import Image
import background from "../../assets/images/background.png";

const styles = {
  backgroundImage: `url(${background})`
};

const IntroSlide = ({ nextSlide }) => (
  <div className="slide first" style={styles}>
    <div className="slide__container">
      <div className="title">Where in the world?</div>
      <div className="body-copy">
        You’re well travelled. You’ve hiked, climbed and seen some of the
        greatest natural wonders on Earth. But where in the world would you see
        the following 14 sights?
      </div>

      <Button event={nextSlide}>Start quiz</Button>
    </div>
  </div>
);
// Prop Validation
IntroSlide.propTypes = {
  nextSlide: PropTypes.func.isRequired
};

export default IntroSlide;
